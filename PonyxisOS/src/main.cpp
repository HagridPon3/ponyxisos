#include "mbed.h"
#include "string.h"
#include "PonyxisOS.h"

PonyxisOS ponyxisos;

Serial kb(PB_6, PA_10); // tx, rx

char dataIN[100];
int pos = 0;
char split[4][10];

void parse(char _data[100])
{
  sscanf(dataIN, "%s %s %s %s", split[0], split[1], split[2], split[3]);
}

void erase()
{
  for (int i = 0; i < 100; i++) {
    dataIN[i] = 0x00;
  }

  for (int i = 0; i < 10; i++) {
    split[0][i] = 0x00;
    split[1][i] = 0x00;
    split[2][i] = 0x00;
    split[3][i] = 0x00;
  }
  pos = 0;
}

int main()
{
  ponyxisos.begin();
  ponyxisos.print("PonyxisOS v0.1 MBED EDITION!!\n");
  ponyxisos.print(">");

  while (1) {
    if (kb.readable()) {
      char in = kb.getc();
      if (in == '\n') {
        parse(dataIN);
        if(!strcmp(split[0],"help")){
          ponyxisos.print("\nhelp - run this screen (work)\n");
          ponyxisos.print("info - show info text (work)");
        }
        if(!strcmp(split[0],"info")){
          ponyxisos.print("\nPonyxisOS v0.1 MBED Framework\n");
          ponyxisos.print("Author: Ponyxis\n");
          ponyxisos.print("MCU: STM32F446REt6\n");
          ponyxisos.print("Build date: ");
          ponyxisos.print(__DATE__);
        }
        erase();
        ponyxisos.print("\n>");
      } else {
        dataIN[pos] = in;
        ponyxisos.print(&dataIN[pos]);
        pos++;
      }
    }
  }
}
