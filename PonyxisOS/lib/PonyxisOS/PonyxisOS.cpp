#include "PonyxisOS.h"

Nextion nextion(PA_0, PA_1);

PonyxisOS::PonyxisOS() {}

void PonyxisOS::begin() { nextion.init(); }

void PonyxisOS::print(char *_data)
{
  size_t _len = strlen(_data);
  for (size_t i = 0; i < _len; i++) {
    _screen[_y][_x] = _data[i];
    _x++;
    if (_data[i] == '\n') {
      Render();
      _actline++;
      _x = 0;
      _y++;
    }
  }
  Render();
}

void PonyxisOS::Render()
{
  switch (_actline) {
  case 0:
    nextion.setText("t0", _screen[0]);
    break;
  case 1:
    nextion.setText("t1", _screen[1]);
    break;
  case 2:
    nextion.setText("t2", _screen[2]);
    break;
  case 3:
    nextion.setText("t3", _screen[3]);
    break;
  case 4:
    nextion.setText("t4", _screen[4]);
    break;
  case 5:
    nextion.setText("t5", _screen[5]);
    break;
  case 6:
    nextion.setText("t6", _screen[6]);
    break;
  case 7:
    nextion.setText("t7", _screen[7]);
    break;
  case 8:
    nextion.setText("t8", _screen[8]);
    break;
  case 9:
    nextion.setText("t9", _screen[9]);
    break;
  case 10:
    nextion.setText("t10", _screen[10]);
    break;
  case 11:
    nextion.setText("t11", _screen[11]);
    break;
  case 12:
    nextion.setText("t12", _screen[12]);
    break;
  case 13:
    nextion.setText("t13", _screen[13]);
    break;
  case 14:
    nextion.setText("t14", _screen[14]);
    break;
  case 15:
    nextion.setText("t15", _screen[15]);
    break;
  case 16:
    nextion.setText("t16", _screen[16]);
    break;
  case 17:
    nextion.setText("t17", _screen[17]);
    break;
  }
}
