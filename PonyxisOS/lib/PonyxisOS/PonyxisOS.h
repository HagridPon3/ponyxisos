#ifndef PonyxisOS_h
#define PonyxisOS_h
#include "Nextion.h"
#include "mbed.h"
#include "string.h"

class PonyxisOS
{
public:
  PonyxisOS();
  void begin();
  void print(char *in);
  void Render();

private:
  char _screen[18][70] = {};
  int _x, _y;
  int _actline = 0;
};
#endif
