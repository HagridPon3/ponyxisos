#ifndef NEXTION_H
#define NEXTION_H

#include "mbed.h"

class Nextion
{
public:
  Nextion(PinName Tx, PinName Rx);
  void init();
  void setText(char _id[3], char _txt[50]);

private:
  Serial lcd;
};

#endif
