#include "Nextion.h"
#include "mbed.h"

Nextion::Nextion(PinName Tx, PinName Rx) : lcd(Tx, Rx) {}

void Nextion::setText(char _id[3], char _txt[50])
{
  char data[60];
  sprintf(data, "%s.txt=\"%s\"%c%c%c", _id, _txt, 0xff, 0xff, 0xff);
  lcd.printf(data);
}

void Nextion::init()
{
  lcd.baud(9600);
  lcd.printf("rest%c%c%c", 0xff, 0xff, 0xff);
  wait(0.2);
}
